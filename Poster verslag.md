# Poster 1: How to disentangle different brain disorders

Deze poster is overladen met tekst en druke illustraties, waardoor het geheel druk en onoverzichtelijk wordt. Jammer genoeg ontbreekt de naam van de maker, waardoor ik niet zeker weet wie de poster heeft gemaakt. Met de duidelijke achtergrondinformatie en onderzoeksdoel kun je snel zien waarvoor het onderzoek is gedaan.

De methode vereist zorgvuldige observatie om de betekenis ervan te begrijpen, maar de flowchart ziet er visueel aantrekkelijk uit. Helaas heb ik over figuur 1 irritatie, aangezien de symptomen niet op alfabetische volgorde zijn geordend, zelfs niet binnen hun eigen categorieČn. Dit zou de grafiek aanzienlijk leesbaarder en bruikbaarder hebben gemaakt.

Figuur 2 daarentegen biedt een heldere illustratie van een mogelijke toepassing van de gemaakte database om snel overzichtelijk informatie te krijgen van verschillende aandoeningen. Helaas is figuur 3 verwarrend, aangezien de grafiek de "proportie" aangeeft op de y-as, maar het is niet duidelijk of het gaat om proportie van personen met of zonder psychose. Dit gebrek aan helderheid belemmert de begrijpbaarheid van de grafiek.

Ondanks deze punten biedt de database die in dit onderzoek is gemaakt veel mogelijkheden voor onderzoek naar hersenaandoeningen. Vooral met toevoegingen zoals genexpressiegegevens en gedetailleerde neuropathologie kan het onderzoeksveld aanzienlijk worden uitgebreid.

# Poster 2: Classifiction of MinION data

Auteur: Carlo van Buiten

Een overzichtelijke poster met een goede balans tussen tekst en illustraties is gemaakt om het doel van dit onderzoek te presenteren: het identificeren van bacteriën in water om ervoor te zorgen dat er geen pathogene of coliforme bacteriën aanwezig zijn. 

Figuur 1 geeft een duidelijk overzicht van de toegepaste pathway in de pipeline om de resultaten te verkrijgen. Deze pipeline is ontworpen voor een efficiëntere detectie binnen een kortere tijdspanne. Om dit te bereiken, is de basecalling verplaatst van de CPU naar de GPU om betere parallelle verwerking van de data mogelijk te maken. Daarnaast is er ook aanzienlijk minder data gebruikt, wat heeft bijgedragen aan de versnelde verwerking.

In tabel 1 worden de testresultaten weergegeven die zijn verkregen om het effect van de datareductie te bepalen. Helaas ontbreekt de informatie over de totale dataset in de tabel. Het is interessant om te weten wat de grenswaarden zijn voor deze bacteriën in het water en of deze grenswaarden zijn gebruikt bij het bepalen van de datareductie. Bovendien vraag ik me af welke methode is gebruikt om te bepalen welke datapunten geschikt waren voor de datareductie.

Al met al, de poster presenteert een gestructureerd onderzoek waarin de pipeline, verbeteringen en testresultaten worden besproken. Het zou waardevol zijn om meer details te krijgen over de grenswaarden, de methode voor datareductie en de totale dataset.

# Poster 3: Classification of movement disorders using handheld video recordings

Auteur: Kasper Notebomen

Deze poster is goed gestructureerd met een evenwichtige verdeling tussen tekst en illustraties. De tekstspacing draagt bij aan een betere leesbaarheid en zorgt voor een rustige uitstraling. De poster geeft een duidelijk overzicht van de gebruikte pipeline, zoals te zien in Figuur 2. Daarnaast wordt in Figuur 3 een overzicht gegeven van de effecten van de dataprocessing, en is er een grafiek met indicatielijnen die aangeven wanneer de hand zich in de start- en eindpositie bevindt.

Het onderzoek heeft als doel om aan de hand van video-opnamen van de vinger-naar-neus test te bepalen of een persoon Early Onset Ataxia, Developmental Coordination Disorder heeft, of geen van beide. Hiervoor wordt een skeletrepresentatie van de patiënt gemaakt op basis van de video. Met behulp van dit skelet kunnen verschillende aspecten worden bepaald, zoals de startpositie, eindpositie en de tijdsduur tussen de posities.

Zodra deze waarden bekend zijn, kan een machine learning-algoritme worden getraind. In Figuur 5 wordt een voorbeeld gegeven van het K-nearest neighbour-algoritme dat is gebruikt. Uit het onderzoek is gebleken dat dit algoritme een nauwkeurigheid van 63,5% behaalde bij het correct classificeren van de gevallen. 

Over het geheel genomen is deze poster van hoge kwaliteit en verdient het de prijs die het heeft ontvangen tijdens de poster terugkomdag. Het zou interessant zijn om te onderzoeken of de classificatie verder verbeterd kan worden door gebruik te maken van andere algoritmes en/of technieken.