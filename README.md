# SLB Jaar 3

## Wat moet worden ingeleverd

- Posterpresentatie stage terugkomdag, + verslag van 2 poster
- Aanwezig bij de Meet and Greet arbeidsmarkt + verslagje van het bedrijf die je gesproken hebt
- Sollicitatietraining bijgewoon en een CV gemaakt
- ILST seminars (x2) + samenvatting
- Bijwonen 2 afstudeerpresentaties + een vraag gesteld en antwoord genoteerd
- LinkedIn profiel
- VOA punten afgetekend
- 2x gesprek gehad per half jaar + een afsluitend eindgesprek vlak voordat het jaar ten einde is

## LinkedIn Profiel

[LinkedIn profiel](https://linkedin.com/in/micha-snippe)
als link niet werkt
linkedin.com/in/micha-snippe