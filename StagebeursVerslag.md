# Stagebeurs Verslag

Verslag van het gesprek met Bioclear Earth over de mogelijkheid van een stage

Op 9 februari had ik de gelegenheid om een persoonlijk gesprek te voeren met een vertegenwoordiger van Bioclear Earth, een vooraanstaand bedrijf op het gebied van bioinformatica en nat-laboratoriumwerk. Tijdens dit gesprek hebben we uitgebreid gesproken over de mogelijkheid om een stage te volgen waarin ik zowel ervaring kon opdoen in het vakgebied van de bioinformatica als werkzaamheden kon verrichten in het nat-laboratorium.

Bioclear Earth is een toonaangevend bedrijf dat gespecialiseerd is in het ontwikkelen van duurzame oplossingen voor milieuvraagstukken door middel van biotechnologie en bioinformatica. Ze richten zich met name op het verbeteren van de bodemkwaliteit, het verwijderen van vervuiling en het bevorderen van duurzame landbouwpraktijken.

Tijdens het gesprek heb ik mijn interesse in zowel bioinformatica als nat-laboratoriumwerk kenbaar gemaakt. De vertegenwoordiger van Bioclear Earth was positief en gaf aan dat het mogelijk zou zijn om een stage te doen waarin ik beide aspecten kon combineren. Uiteraard zou dit afhangen van mijn kennis en ervaring op deze gebieden, maar met wat overleg en afstemming zou er een passend stageprogramma kunnen worden samengesteld.

Kortom, het gesprek met Bioclear Earth was zeer positief. Het bedrijf toonde interesse in mijn wens om zowel in de bioinformatica als op het nat-lab ervaring op te doen en gaf aan dat dit mogelijk zou zijn met wat overleg. Ik ben erg enthousiast over de mogelijkheid om een stage bij Bioclear Earth te volgen, gezien hun reputatie, expertise en betrokkenheid bij duur